object frmChangeCRC: TfrmChangeCRC
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1057#1084#1077#1085#1072' '#1082#1086#1085#1090#1088#1086#1083#1100#1085#1086#1081' '#1089#1091#1084#1084#1099
  ClientHeight = 144
  ClientWidth = 265
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblCRC: TLabel
    Left = 8
    Top = 8
    Width = 165
    Height = 13
    Caption = #1044#1083#1103' '#1088#1072#1089#1095#1077#1090#1072' CRC '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1090#1100':'
  end
  object txtInfo: TLabel
    Left = 8
    Top = 27
    Width = 249
    Height = 78
    AutoSize = False
    Caption = 
      'FM 10 '#1080' '#1088#1072#1085#1077#1077' - 255 FM 11 / 12 [EUR] - 250 FM 11 / 12 [RUS] - 20' +
      '5 FM 11 / 12 [RUS], patch 12.1 - 250'
  end
  object btnUpdate: TButton
    Left = 8
    Top = 111
    Width = 75
    Height = 25
    Caption = #1054#1073#1085#1086#1074#1080#1090#1100
    ModalResult = 1
    TabOrder = 0
  end
  object btnClose: TButton
    Left = 182
    Top = 111
    Width = 75
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 2
    TabOrder = 1
  end
  object txtCRC: TEdit
    Left = 184
    Top = 5
    Width = 73
    Height = 21
    TabOrder = 2
    OnChange = txtCRCChange
  end
end
